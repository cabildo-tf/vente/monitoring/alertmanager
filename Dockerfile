ARG ALERTMANAGER_VERSION=v0.21.0
FROM prom/alertmanager:${ALERTMANAGER_VERSION}

USER root
COPY rootfs /
RUN chown -R nobody:nogroup /etc/alertmanager

USER nobody

ENTRYPOINT ["/usr/bin/entrypoint.sh"]