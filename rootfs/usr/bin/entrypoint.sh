#!/bin/sh

CONFIG_PATH="/etc/alertmanager"

cat "${CONFIG_PATH}/alertmanager-tmpl.yml" \
	| sed "s@\${ALERTMANAGER_BOT_URL}@${ALERTMANAGER_BOT_URL}@g" \
	> ${CONFIG_PATH}/alertmanager.yml

cat "${CONFIG_PATH}/alertmanager.yml" 


# Force all args into alertmanager
if [[ "$1" = 'alertmanager' ]]; then
  shift
fi

exec alertmanager --config.file="${CONFIG_PATH}/alertmanager.yml" \
				  --storage.path=/alertmanager \
				  --web.external-url="${WEB_EXTERNAL_URL:-http://localhost:${PORT}}" \
	  			  --web.route-prefix="${WEB_ROUTE_PREFIX:-/}" \
	        	  "$@"
